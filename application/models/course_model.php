<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Course_model extends CI_Model
    {
        public function get($id = "")
        {
            if(empty($id)):
            //    return $this->db->query("SELECT * FROM courses")->result();
                return $this->db->get('courses')->result();

                /**
                    num_rows => count number of rows
                    num_fields => count number of columns
                */
            else:
                return $this->db->where(['id'=>$id])->get('courses')->result();
            endif;
        }
        public function insert($data){
            $this->db->insert('courses',$data);
        }
        public function update($data,$id){
            $this->db->where(['id'=>$id])->update('courses',$data);
        }
        public function delete($id)
        {
            $this->db->where(['id'=>$id])->delete('courses');
        }

    }