<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit</title>
    <style>
        *{
            margin:0;
            padding:0;
        }
        input{
            display: block;
            /* border: 5px solid red; */
            margin-top:2%;
        }
        #container{
            /* border: 5px solid yellow; */
            padding-left:20%;
            padding-right:20%;
            text-align:center;           
        }
        form{
            /* border: 5px solid; */
            padding-left:40%;
        }
        div > input{
            position:relative;
            left:9%;
            
        }
    </style>
</head>
<body>
    <div id="container">
        <h2>Edit your course!</h2>
        <?php 
                $attributes =array(
                    'id'=>'login_form',
                    'class'=>'form_horizontal'
                );
            ?>     
        <?php  echo form_open('course/update/'.$id,$attributes);?>
            <input value ='<?php echo $course[0]->coursename ;?>' type="text" name="coursename" id="">
            <div>
                <input type="submit" value="Edit">
            </div>
            </form>
    </div>
</body>
</html>