
    
        <h2>Register your course!</h2>
            <?php 
                $attributes =array(
                    'id'=>'login_form',
                    'class'=>'form_horizontal'
                );
            ?>     
        <?php  echo form_open('course/store',$attributes);?>
        <div class="form-group">
            <?php echo form_label('Course name'); ?>
            <?php 
                $data =  array(
                    'class' => 'form-control',
                    'name' => 'coursename',
                    'placeholder' => 'Enter Course Name'
                );
            ?>
            <?php echo form_input($data); ?>
        </div>
        <div class="form-group">
            <?php 
                $data =  array(
                    'class' => 'btn btn-primary',
                    'value'=> 'Register'
                );
            ?>
            <?php echo form_submit($data); ?>
        </div>

        <?php echo form_close(); ?>
            

     