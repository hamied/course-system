<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css">
    <script src="<?php echo base_url()?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="col-xs-3">
        <?php $this->load->view($signup) ?>
        </div>
        <div class="col-xs-9">
            <?php $this->load->view($index,['courses'=>$courses]) ?>
        </div>
    </div>
</body>
</html>