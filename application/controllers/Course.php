<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Course extends CI_Controller{
        public  function index()
        {
            $courses =  $this->Course_model->get();
            $this->load->view('layouts/main',['courses' => $courses,'index'=> 'course/index','signup'=>'course/signup']);
        }
        public function store(){ 
           $this->form_validation->set_rules('coursename','Coursename','trim|required|min_length[2]');
           $coursename =  $this->input->post('coursename');
         $this->Course_model->insert(['coursename'=>$coursename]);
                 redirect('course')->with('Added Sucessfully');
               // echo "hello";
           

        }
        public function show($id)
        {
            $course = $this->Course_model->get($id);
            $this->load->view('course/show',['course' => $course]);
        }
        public function edit($id)
        {
            $course = $this->Course_model->get($id);
            $this->load->view('course/edit',['course' => $course,'id'=>$id]);
        }
        // public function store(Request $request)
        // {
        //    if( $this->Course_model->insert($request['coursename'])){
        //        return $this->index();
        //    }
        // }
        public function update($id)
        {
            $coursename =  $this->input->post('coursename');
           $this->Course_model->update(['coursename' => $coursename],$id);
               redirect('course')->with('Updated Sucessfully');
           
        }
        // public function destroy($id)
        // {
        //     if($this->Course_model->delete($id))
        //     {
        //         return $this->index();
        //     }
        // }
        public function signin(){
            return $this->load->view('course/login');
        }

        public function login(){
            $course = $this->input->post('coursename');
            $courses = $this->Course_model->get();
            if(!empty($course)):
                foreach($courses as $object){
                    if($course == $object->coursename){
                        redirect('course/show/'.$object->id);
                    }
                
                }
            endif;
            die('Not foound');

        }
        
    }
   


